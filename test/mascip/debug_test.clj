(ns mascip.debug-test
  (:require [clojure.test :refer :all]
            [mascip.debug :refer :all]))

;;;; dbg

(deftest dbg-simple
  (testing "the value 3"
    (is (= (with-out-str (dbg 3))
           "dbg: 3 => 3\n")))

  (testing "(+ 1 2)"
    (is (= (with-out-str (dbg (+ 1 2)))
           "dbg: (+ 1 2) => 3\n")))
)

(deftest dbg-inside
  (testing "the value 3"
    (is (= (with-out-str (* 7 (dbg 3)))
           "dbg: 3 => 3\n")))

  (testing "(+ 1 2)"
    (is (= (with-out-str (* 7 (dbg (+ 1 2))))
           "dbg: (+ 1 2) => 3\n")))
)

(deftest dbg-outside
  (testing "(+ 1 2)"
    (is (= (with-out-str (dbg (+ 1 (inc 1))))
           "dbg: (+ 1 (inc 1)) => 3\n")))
)

(deftest dbg-both-inside-and-outside
  (testing "(+ 1 2)"
    (is (= (with-out-str (* 7 (dbg (+ 1 (inc 1)))))
           "dbg: (+ 1 (inc 1)) => 3\n")))
)

;;;; c, cs

(deftest c-passes-the-data
  (testing "passes the value 3"
     (is (= 3
            (-> 3 (c "some comment")))))

  (testing "[1 2]"
     (is (= [1 2]
            (-> [1 2] (c "some comment")))))
)

(deftest cs-prints-the-data
  (testing "passes the value 3"
     (is (= 3
            (-> 3 (cs "some comment")))))

  (testing "prints the value 3"
     (is (= "some comment\n3\n"
            (with-out-str (-> 3 (cs "some comment"))))))
)

;;;; cc, ccs
(deftest cc-passes-the-data
  (testing "passes the value 3"
     (is (= 3
            (->> 3 (cc "some comment")))))

  (testing "[1 2]"
     (is (= [1 2]
            (->> [1 2] (cc "some comment")))))
)

(deftest ccs-prints-the-data
  (testing "passes the value 3"
     (is (= 3
            (->> 3 (ccs "some comment")))))

  (testing "prints the value 3"
     (is (= "some comment\n3\n"
            (with-out-str (->> 3 (ccs "some comment"))))))

)

;;;; showc
(deftest showc-prints-the-data
  (testing "passes the value 3"
     (is (= 3
            (showc
              (->> 3 (cc "some comment"))
            ))))

  (testing "prints the value 3"
     (is (= "some comment\n3\n"
            (with-out-str (showc
                            (->> 3 (cc "some comment"))
                          )))))
)

(deftest showc-complex-example
  (testing "prints with (showc)"
    (is (= (with-out-str (do
     (-> (+ 1 2) (c "WILL NOT BE PRINT"))
     (showc
       (->> [1 2]               (cc "Input")
            reverse             (cc "Reversed")
            (map (partial + 3)) (cc "Add 3")
       )
      )
     (->> [1 2] (cc "NOT PRINTED EITHER"))
     ))
"Input
[1 2]
Reversed
(2 1)
Add 3
(5 4)
")))

  (testing "passes the value"
    (is
(= (do
     (-> (+ 1 2) (c "WILL NOT BE PRINT"))
     (showc
       (->> [1 2]      (cc "Input")
            reverse    (cc "Reversed")
            (map (partial + 3)) (cc "Add 3")
       )
      )
     (->> [1 2] (cc "NOT PRINTED EITHER"))
   [1 2]
)))))

(deftest nested-function
  (testing "prints in nested functions"
    (letfn [(nested-fn [n]
              (-> n inc (c "Nested inc")))]
      (is (= (with-out-str
        (showc
          (->> 1          (cc "Input")
               nested-fn  (cc "Nested function")
               str        (cc "->str")
        )))
"Input
1
Nested inc
2
Nested function
2
->str
\"2\"
")))))

(deftest lazy-map
  (testing "prints in correct order when using a map (who returns a lazy sequence)"
    (letfn [(nested-fn [n]
              (-> n inc (c "Nested inc")))]
      (is (= (with-out-str
        (showc
          (->> [1 2]           (cc  "Input")
               (map nested-fn) (ccl "Nested function")
               reverse         (cc  "Reverse")
        )))
"Input
[1 2]
Nested inc
2
Nested inc
3
Nested function
(2 3)
Reverse
(3 2)
")))))

(deftest no-showc-stops-printing
  (testing "Does not print in no-showc scope"
    (letfn [(nested-fn [n]
              ((no-showc
                (-> n inc (c "INSIDE no-showc, WILL NOT GET PRINTED")))))]
      (is (= (with-out-str
        (showc
          (->> 1          (cc "Input")
               nested-fn  (cc "Nested function")
               str        (cc "->str")
        )))
"Input
1
Nested function
2
->str
\"2\"
")))))

(deftest no-showc-inside-a-chain
  (testing "Does not print in no-showc scope"
    (letfn [(nested-fn [n]
              (println "THIS GETS PRINTED")
              (-> n inc (c "INSIDE no-showc, WILL NOT GET PRINTED")))]
      (is (= (with-out-str
        (showc
          (->> 1                       (cc "Input")
               ((no-showc nested-fn))  (cc "Nested function")
               str                     (cc "->str")
        )))
"Input
1
THIS GETS PRINTED
Nested function
2
->str
\"2\"
")))))

