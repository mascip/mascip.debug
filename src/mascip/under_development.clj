(ns mascip.under-development
    (:require
      [mascip.debug :refer :all]

      [clojure.string :as s]
      [clojure.data :as d]
))

(defn diff [s1 s2]
  "To find where is the difference between two strings:
  (diff \"fabler\" \"cable\") => \"f    r"\"
  (->> [s1 s2]
       (map (partial into []))  (cc "str->chars")
       (apply d/diff)           (cc  "diff")
       (first)                  (cc "only keep diff on first string")
       (map #(if (= % nil) " " %))(s/join) (cc "chars->str. nil elements are replaced by a space")
))

