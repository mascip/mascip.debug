(ns mascip.debug
  (:require [clojure.string :as s]
            [clojure.pprint  :refer :all]
            [robert.hooke   :refer :all]
  ))


; TODO: let user choose their own printer => use a simple one for testing this module

;;;; (dbg) (and (pdbg)
(defmacro dbg
  "Try this:
  (+ (* 2 3) (dbg (* 8 9)))
  => dbg: (* 8 9) => 72"
  [x]
  `(let [x# ~x] (println "dbg:" '~x "=>" x#) x#))

(defmacro pdbg
  "Like (dbg) but prints with colors"
  [x]
  `(let [x# ~x] (do (println "--")(pprint '~x)(println "->")(pprint x#) (println "--") x#)))


;;;; (c), (cc), (cl), (ccl)

(defn c [data s]
  "A comment that helps you debug your -> thread chains. Is takes data in and passes the same data out. For example:
  (-> [1 2] (c \"The vector\") reverse (c \"Reversed\"))
  => [2 1]
  It's just like a comment... but you can turn it into (cs) so it will print the comment followed by the data sctructure passed in. You can also use (showc) to 'switch on' all the (c) functions in a given scope"
  data)

(defn cc "Same as (c) but for ->> thread chains."
  [s data]
  (c data s))

(defn cl "Same as (c), but for lazy data. See the doc for (cls)."
  [data s]
  data)

(defn ccl "Same as (cl) but for (cc) rather than (c)"
  [s data]
  (cl data s))


; Hooks and functions for printing in a given scope

(defn c-print
  "Use (showc) to add it as a hook on the (c) function, to 'switch it on' and make it behave like (cs) as long as the hook is added."
  [f data s]
  (println s)
  (pprint data)
  data)

(defn cl-print "Same as (c-print) but for lazy data. See the doc for (cls)"
  [f data s]
  (let [d (doall data)] ; Evaluate data if it's lazy
    (c-print f data s)))

(defmacro showc
  "Makes (c) behave like (cs) and (cc) like (ccs): they now print the comment and data.
  But only in the scope of (showc).
  You can use (no-showc) to cancel this behavior in a sub scope"
  [& forms]
   `(robert.hooke/with-scope
     (robert.hooke/add-hook #'c #'c-print)
     (robert.hooke/add-hook #'cl #'cl-print)
     ; Just the same forms as they were
     ~@forms
))

(defmacro no-showc [& forms]
  "MUST ALWAYS BE ENCLOSED IN TWO PAIRS OF PARENTHESES: ((no-showc)). And must always be used from within a (showc) scope.
  In the given scope, don't let (c) behave like (cs), nor (cc) behave like (ccs): remove the printing hooks.
  Can be used either from outside a threading chain, or by just enclosing one function in the threading chain, line this:
    (showc
      (->> 1          (cc \"Input\")
           ((no-showc nested-fn))  (cc \"Nested function\")
           str        (cc \"To string\")))
  "
  `(fn
     ([]
      (robert.hooke/with-hooks-disabled c
        (robert.hooke/with-hooks-disabled cl
          ; Just the same forms as they were
          ~@forms
      )))
     ([data-in#]
    (robert.hooke/with-hooks-disabled c
      (robert.hooke/with-hooks-disabled cl
        ; Just the same forms as they were
        (~@forms data-in#)
)))))

(defmacro printer [& forms]
  `(println ~@forms))

(defmacro adder [s]
  `(inc ~s))

(defn cs [data s]
  "A debugging tool for ->> thread chains. Takes data in, returns the same data out, and prints your comment followed by the data. Can be switched off by turning it into (c)"
  (c-print c data s))

(defn ccs [s data]
  "Same as (cs)but for -> thread chains"
  (cs data s))

(defn cls [data s]
  "Same as (cs) but for lazy data. For instance when I do
  (-> some-data (map some-fn) (cs \"some comment\"))
  the (map) will return a lazy list, which gets evaluated only when the list is printed. This makes the text \"some comment\" be printed before (some-fn) gets evaluated, and the data be printed after it's evaluated. As a consequence, if (some-fn) prints anything, this text will end up between \"some comment\" and the relevant data.

  (cls) solves this problem by evaluating the lazy list before doing anything else. This is good for printing purpose, but it means that you are now manipulating a list instead of a lazy list - which might not be what you want."
  (cl-print cl data s))

(defn ccls [s data]
  "Same as (cls) but for (cc) rather than (c)"
  (cls data s))
