# mascip.debug

Debugging functions for printing data transformations on a "need to see" basis, along threading chains `->>` and function composition chains `(some (function (and (another`.

## Usage

    (showc ; If you delete this line, nothing will be printed
      (-> [1 2 3]              (c "Input")
          (reverse)            (c "Reverse")
          (conj "some text")   (c "Add text")
    ))
    => Input
       [1 2 3]
       Reverse
       (3 2 1)
       Add text
       ("some text" 3 2 1)

## The (c) functions and (showc)

I used to add comments at the end of complex lines, in the middle of a threading chain `->>`.

        ; What this line does

Now have this instead of the comments:

        (c "What this line does")

It doesn't do anything, it's like a comment. But if you enclose it inside a `(showc   )` block, then it prints you comment, followed by the printed data structure obtained on that line:

    (showc (-> [1 2] (c "Input")))
    => Input
       [1 2]

There is `c` for `->` chains, and `cc` for `->>` chains. They do the same thing, but take arguments in a different order.

Under the hood, `showc` added a hook on the `c` function, to make it print the data. I used [robert.hooke](https://github.com/technomancy/robert-hooke/) for this purpose (but you don't need to know that).


## The (cs) functions, to show only one target line

You can also turn `(c "text")` into `(cs "text")` to get that particular statement to print its text and the data.

    (-> 1
        inc (c "first increment")
        inc)
    => 3

    ; Just add an "s" to get that line displayed
    (-> 1
        inc (cs "first increment")
        inc)
    => first increment
       2
       3

There is also `ccs` for printing a `cc` statement.

# The (cl) functions, to display lazy data structures

If some lines return a lazy data structure, you can use `cl` (or `ccl`) to have the data evaluated before the anything gets printed (ATTENTION: you will not always want this!).
If you don't use `cl` and `ccl` you will get things like this:

    (defn some-fn [n]
      (println "Making a mess")
      (inc n))

    (->> [1 2]
         (map some-fn) (ccs "Just a map:"))
    => Just a map:
       Making a mess
       Making a mess
       (2 3)
    ; The mess got printed BEFORE the function got evaluated

    ; Just add an "l" to make it print things in the right order
    (->> [1 2]
         (map some-fn) (ccls "Just a map:"))
    => Making a mess
       Making a mess
       Just a map:
       (2 3)

In this family of functions there are `cl`, `ccl`, `cls`, `ccls`.


# (showc) works as expected with nested functions

Example:

    (defn nested-fn [n]
      (-> n
          inc    (c "Nested inc")
          (* 4)  (c "Mutiply by 4")))
    
    (showc
      (->> 1          (cc "Input")
           nested-fn  (cc "Nested function")
           str        (cc "To string")))
    => Input
       1
       Nested inc
       2
       Multiply by 4
       8
       Nested function
       8
       To string
       "8"

That's great! But when you start to have `(c)` statements in various parts of your code, you might not always want to print all nested statements. That's where `no-showc`comes in handy.


# ((no-showc)), to not print the (c) statements in the given scope

There are two ways to use `((no-showc))`:

In the chain itself (be careful, it must enclose the right thing)

    (showc
      (->> 1          (cc "Input")
           ((no-showc nested-fn))  (cc "Nested function")
           str        (cc "To string")))
    => Input
       1
       Nested function
       8
       To string
       "8"

In the nested function:

    (defn nested-fn [n]
      ((no-showc
      (-> n
          inc    (c "Nested inc")
          (* 4)  (c "Mutiply by 4"))))
    
    (showc
      (->> 1          (cc "Input")
           nested-fn  (cc "Nested function")
           str        (cc "To string")))
    => Input
       1
       Nested function
       8
       To string
       "8"

Due to its implementation, `((no-showc))` must **always** be enclosed by two sets of parentheses, or its content won't be evaluated. It must also always be inside a `(showc   )` scope. If someone comes up with a better implementation, please send me a pull request!
The good thing is: it works as it is :-) It could be improved by not needing to enclose it in two sets of parentheses. Want to improve your macro writing skills? Give it a go!


# The (dbg) function

A nifty debugging function, to be used inside your call chains.

    (* 7 (dbg (+ 1 (inc 1)))))
    => dbg: (+ 1 (inc 1)) => 3

If you have good tools, most of the time you should be using the REPL instead of `dbg`. Nonetheless, `dbg` is sometimes very useful within the REPL.

# General Debugging advice:

Please go and read [this file](https://bitbucket.org/mascip/debugging-clojure-howto) where I keep up postto data a list of ideas of how to debug in Clojure:

 - useful libraries,
 - methodologies,
 - tools,
 - useful tips

And contribute if there's something missing :)

## Acknowledgements

 - The `c` function is inspired by Micheal Feather's c function for Ruby, in his article [Literate Chains for Functional Programming](https://michaelfeathers.silvrback.com/literate-chains-for-functional-programming)

 - `dbg` was found in the article [Conditioning the REPL](http://www.learningclojure.com/2010/03/conditioning-repl.html) by John Lawrence Aspden"

Don't hesitate to indicate issues and send pull requests.


## License

CopyLeft 2014

Distributed under the Eclipse Public License version 1.0.
